import { Directive, ElementRef, Renderer2, HostListener, HostBinding, OnInit } from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit{

  @HostBinding('style.backgroundColor') backgroundColor: string = 'transparent';

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

  ngOnInit() {
    // this.renderer.setStyle(this.elementRef.nativeElement, 'backgroundColor', 'bisque');
  }

  @HostListener('dragover') mouseover() {
    // this.renderer.setStyle(this.elementRef.nativeElement, 'backgroundColor', 'bisque');
    this.backgroundColor = 'bisque';
  }
  
  @HostListener('mouseleave') mouseleave() {
    // this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', 'transparent');
    this.backgroundColor = 'transparent';
  }

}
