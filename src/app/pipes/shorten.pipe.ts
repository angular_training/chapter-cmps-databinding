import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {

  transform(value: any, length: number, endChar: string='...'): any {
    if(!value) {
      return '';
    }

    if(value.length < length) {
      return value;
    }

    return value.substring(0, length).concat(endChar)
  }

}
