import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  // pure: false
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filterStr: string, property: string): any {
    if(value.length == 0 || filterStr.length == 0) {
      return value;
    }

    let results = [];
    return value.filter((el) => el[property].toUpperCase() === filterStr.toUpperCase());
  }

}
