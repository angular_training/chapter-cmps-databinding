import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  styles: [
    `h3 {
      color: brown
    }
    h4 {
      color: coral
    }`
  ]
})
export class AppComponent {
  name = 'Mohamed Hamidi';
  switchValue = 10;
}
