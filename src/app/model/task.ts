export class Task {
  constructor(private taskId: number, private taskTitle: string, 
    private taskStatus: string, private creationDate: Date, private accomplished: boolean) {

  }
}
