import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css'],
  styles: [
    `
      .completed {
        text-decoration: line-through
      }
    `
  ]
})
export class TaskComponent implements OnInit {

  taskId: number = 9;
  taskTitle: string = 'Learn Angular';
  taskStatus = 'In Progress';

  constructor() { 
    this.taskStatus = Math.random() * 10 > 5 ? 'In Progress' : 'Completed';
  }

  ngOnInit() {
  }

  getTaskStatus() {
    return this.taskStatus;
  }

  getColor() {
    return this.taskStatus === 'In Progress' ? 'aqua' : 'aquamarine';
  }

  isTaskCompleted() {
    return this.taskStatus === 'Completed'; 
  }

}
