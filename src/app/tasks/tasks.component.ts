import { Component, OnInit } from '@angular/core';
import { Task } from '../model/task';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  allowAddTask: boolean = false;
  taskCreationStatus = 'No task was created';
  taskTitle = '';
  taskCreated: boolean = false;
  tasks = ['Learn Angular', 'Getting started GraphQL'];
  pipeTasks: Task[] = [
    new Task(1, 'Learn Angular', 'In Progress', new Date(2019, 3, 25), false),
    new Task(2, 'Getting started GraphQL', 'Open', new Date(2018, 11, 10), false),
    new Task(3, 'Start learning NodeJs', 'Open', new Date(2018, 10, 1), true),
    new Task(4, 'Learn ReactJs', 'Archiveded', new Date(2018, 1, 17), true)
  ];
  filterStr: string = '';
  trainingStatus = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('In Progress..')
    }, 2000);
  })

  constructor() {
    setTimeout(
      () => {
        this.allowAddTask = true;
      }, 2000);
   }

  ngOnInit() {
  }

  onCreateTask() {
    this.taskCreated = true;
    this.tasks.push(this.taskTitle);
    this.taskCreationStatus = `task with title "${this.taskTitle}" was created!`;
  }

  onUpdateTaskTitle(event: any) {
    this.taskTitle = event.target.value;
  }

  onAddTaskForPipes() {
    this.pipeTasks.push(new Task(1, 'Learn Something', 'Open', new Date(), false));
  }

}
